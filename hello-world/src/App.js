import React from 'react';
import { BrowserRouter, Routes, Route } from "react-router-dom"
import Cinema from './pages/Cinema';
import Contact from './pages/Contact';
import Film from './pages/Film';
import Geographie from './pages/Geographie';
import Home from './pages/Home';
import Notfound from './pages/Notfound';

const App = () => {
  return (
    
    <BrowserRouter>
      <Routes>
        <Route path="/" element={ <Home /> } />
        <Route path="/cinema" element={ <Cinema /> } />
        <Route path="/contact" element={ <Contact /> } />
        <Route path="/film" element={ <Film /> } />
        <Route path="/geographie" element={ <Geographie /> } />
        <Route path="*" element={ <Notfound /> } />
      </Routes>
    
    </BrowserRouter>
  );
};

export default App;

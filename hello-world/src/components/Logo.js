import React from 'react';

const Logo = () => {
    return (
        <div>
            <img src="./assets/img/logo192.png" alt="☻" />
        </div>
    );
};

export default Logo;
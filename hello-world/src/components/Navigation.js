import React from 'react';
import { NavLink } from 'react-router-dom';

const Navigation = () => {
    return (
        <nav>
            <ul>
                <NavLink to="/">
                    <li>Home</li>
                </NavLink>
                <NavLink to="/geographie">
                    <li>Géographie</li>
                </NavLink>
                <NavLink to="/cinema">
                    <li>Cinéma</li>
                </NavLink>
                <NavLink to="/film">
                    <li>Film</li>
                </NavLink>
                <NavLink to="/contact">
                    <li>Contact</li>
                </NavLink>

            </ul>
        </nav>
    );
};

export default Navigation;
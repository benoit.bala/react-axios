import React from 'react';
import Logo from '../components/Logo';
import Navigation from '../components/Navigation';

const Film = () => {
    return (
        <div>
            <Logo />
            <Navigation />
        </div>
    );
};

export default Film;
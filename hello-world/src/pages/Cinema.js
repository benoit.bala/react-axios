import React from 'react';
import Logo from '../components/Logo';
import Navigation from '../components/Navigation';


const Cinema = () => {
    return (
        <div>
            <Logo />
            <Navigation />
        </div>
    );
};

export default Cinema;
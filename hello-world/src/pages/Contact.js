import React from 'react';
import Logo from '../components/Logo';
import Navigation from '../components/Navigation';

const Contact = () => {

    function sayHello(){
        alert("Hello world");
    }
    return (
        <div>
            <Logo />
            <Navigation />
            <div>
                <button onClick={sayHello}>Click</button>
            </div>
        </div>
    );
};

export default Contact;
import React from 'react';
import Logo from '../components/Logo';
import Navigation from '../components/Navigation';
import Pays from '../components/Pays';

const Geographie = () => {
    return (
        <div>
            <Logo />
            <Navigation />
            <h1>Géographie</h1>
            <Pays />
        </div>
    );
};

export default Geographie;